import logging

from discord.ext import commands

from .coins import AccountType
from .common import Cog, SayException

log = logging.getLogger(__name__)


class Migrator(Cog, requires=["coins"]):
    """Migrator"""

    async def _up(self, table: str, pkey_col: str, new_uid, old_uid):
        res = await self.pool.execute(
            f"""
        UPDATE {table} SET {pkey_col} = $1 WHERE {pkey_col} = $2
        """,
            new_uid,
            old_uid,
        )

        log.debug(f"updating tbl={table} pkey_col={pkey_col} res={res!r}")

    @commands.command()
    @commands.is_owner()
    async def umigrate(self, ctx, old_uid: int, new_uid: int):
        """User migrate.

        Will migrate all supported tables to a new user.

        Useful if the old account was deleted.
        """
        await ctx.send("ok")

        amount = await self.pool.fetchval(
            """
        SELECT amount FROM accounts WHERE account_id = $1
        """,
            old_uid,
        )

        if amount is None:
            raise SayException("can not migrate without josecoin " "account on old id")

        await self.pool.execute(
            """
        INSERT INTO accounts (account_id, account_type, amount)
        VALUES ($1, $2, $3)
        """,
            new_uid,
            AccountType.USER,
            amount,
        )

        await self._up("wallets", "user_id", new_uid, old_uid)

        await self._up("transactions", "sender", new_uid, old_uid)
        await self._up("transactions", "receiver", new_uid, old_uid)

        await self._up("steal_points", "user_id", new_uid, old_uid)
        await self._up("steal_cooldown", "user_id", new_uid, old_uid)
        await self._up("steal_grace", "user_id", new_uid, old_uid)

        await self._up("steal_history", "thief", new_uid, old_uid)
        await self._up("steal_history", "target", new_uid, old_uid)

        await self._up("taxreturn_cooldown", "user_id", new_uid, old_uid)
        await self._up("relationships", "user_id", new_uid, old_uid)

        await self._up("restrains", "user1", new_uid, old_uid)
        await self._up("restrains", "user2", new_uid, old_uid)

        await self._up("badge_users", "user_id", new_uid, old_uid)

        await self._up("starboard", "author_id", new_uid, old_uid)
        await self._up("starboard_starrers", "starrer_id", new_uid, old_uid)

        await self._up("profile_desc", "user_id", new_uid, old_uid)
        await self._up("lottery_tickets", "user_id", new_uid, old_uid)
        await self._up("lottery_cooldowns", "user_id", new_uid, old_uid)

        await self.pool.execute(
            """
        DELETE FROM accounts WHERE account_id = $1
        """,
            old_uid,
        )

        await ctx.ok()


async def setup(bot):
    await bot.add_jose_cog(Migrator)
