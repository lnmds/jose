import logging
import random
import urllib.parse
import collections
from typing import List

import aiohttp
import discord

from discord.ext import commands
from .common import Cog

log = logging.getLogger(__name__)


class BooruError(Exception):
    pass


class BooruClient:
    """Generic API client for image boorus."""

    url = None
    dict_posts_field = None

    @classmethod
    def transform_file_url(cls, url):
        return url

    @classmethod
    def normalize_post(cls, post) -> dict:
        # by default, normalize urls in file_url
        post["file_url"] = cls.transform_file_url(post["file_url"])
        return post

    @classmethod
    async def request_posts(cls, bot, tags: List[str], *, limit: int = 15):
        assert cls.url is not None

        headers = {"User-Agent": "Yiffmobile v2 (José, https://gitlab.com/luna/jose)"}
        safe_tags = urllib.parse.quote(" ".join(tags), safe="")

        result = None
        async with bot.session.get(
            f"{cls.url}&limit={limit}&tags={safe_tags}", headers=headers
        ) as resp:
            assert resp.status == 200
            result = await resp.json()

        # result can be anything, but we need to normalize it so that
        # it can be used by the consumer code. normalization involves
        # returning just a list of dicts containing specific keys.

        if cls.dict_posts_field is not None:
            result = result[cls.dict_posts_field]

        # for each post, normalize it
        return [cls.normalize_post(post) for post in result]


class E621Booru(BooruClient):
    url = "https://e621.net/posts.json?"
    url_post = "https://e621.net/post/show/{0}"
    dict_posts_field = "posts"

    @classmethod
    def normalize_post(self, post):
        all_tags = []
        for tag_type in post["tags"]:
            tags_in_type = post["tags"][tag_type]
            all_tags.extend(tags_in_type)

        tags_as_string = " ".join(all_tags)

        return {
            "id": str(post["id"]),
            "author": post["tags"]["artist"][0],
            "tags": tags_as_string,
            "file_url": post["file"]["url"],
            "score": post["score"]["total"],
            "fav_count": post["fav_count"],
        }


class HypnohubBooru(BooruClient):
    url = "https://hypnohub.net/post/index.json?"
    url_post = "https://hypnohub.net/post/show/{0}"

    @classmethod
    def transform_file_url(cls, url):
        return url.replace(".net//", ".net/")


class GelBooru(BooruClient):
    url = "https://gelbooru.com/index.php?page=dapi&s=post&json=1&q=index"
    url_post = "https://gelbooru.com/index.php?page=post&s=view&id={0}"

    @classmethod
    def normalize_post(self, post):
        return {**post, **{"author": post["owner"]}}


class NSFW(Cog, requires=["config"]):
    """NSFW commands.

    Fetching works on a "non-repeataibility" basis (unless
    the bot restarts). This means that with each set of tags
    you give for José to search, it will record the given post
    and make sure it doesn't repeat again.
    """

    def __init__(self, bot):
        super().__init__(bot)
        self.repeat_cache = collections.defaultdict(dict)

    def key(self, tags):
        return ",".join(tags)

    def mark_post(self, ctx, tags: list, post: dict):
        """Mark this post as seen."""
        cache = self.repeat_cache[ctx.guild.id]

        k = self.key(tags)
        used = cache.get(k, [])
        used.append(post["id"])
        cache[k] = used

    def filter(self, ctx, tags: list, posts):
        """Filter the posts so we get the only posts
        that weren't seen."""
        cache = self.repeat_cache[ctx.guild.id]
        used_posts = cache.get(self.key(tags), [])
        return list(filter(lambda post: post["id"] not in used_posts, posts))

    async def booru(self, ctx, booru: BooruClient, tags: List[str]):
        """Call a booru and give a post, if any."""
        if ctx.guild is None:
            raise self.SayException(
                "Due to the requirement of JoséCoin on the NSFW commands, "
                "they are only available on non-DM channels."
            )

        if ctx.channel.topic and "[no_nsfw]" in ctx.channel.topic:
            return

        await self.jcoin.pricing(ctx, self.prices["API"])

        try:
            # grab posts
            posts = await booru.request_posts(ctx.bot, tags)
            posts = self.filter(ctx, tags, posts)

            if not posts:
                return await ctx.send(
                    "Found nothing.\n"
                    "(this can be caused by an exhaustion "
                    f"of the tags `{ctx.prefix}help NSFW`)"
                )

            # grab random post
            post = random.choice(posts)

            self.mark_post(ctx, tags, post)

            post_id = post.get("id")
            post_author = post["author"]
            log.info("%d posts from %s, chose %s", len(posts), booru.__name__, post_id)

            tags = (post["tags"].replace("_", "\\_"))[:500]

            # add stuffs
            embed = discord.Embed(title=f"Possible artist: {post_author}")
            embed.set_image(url=post["file_url"])
            embed.add_field(name="Tags", value=tags)
            embed.add_field(name="URL", value=booru.url_post.format(post_id))

            # hypnohub doesn't have this
            if "fav_count" in post and "score" in post:
                embed.add_field(
                    name="Votes/Favorites",
                    value=f"{post['score']} votes, " f"{post['fav_count']} favorites",
                )

            # send
            await ctx.send(embed=embed)
        except BooruError as err:
            raise self.SayException(f"Error while fetching posts: `{err!r}`")
        except aiohttp.ClientError as err:
            log.exception("nsfw client error")
            raise self.SayException(f"Something went wrong. Sorry! `{err!r}`")

    @commands.command()
    @commands.is_nsfw()
    async def e621(self, ctx, *tags):
        """Randomly searches e621 for posts."""
        async with ctx.typing():
            await self.booru(ctx, E621Booru, tags)

    @commands.command(aliases=["hh"])
    @commands.is_nsfw()
    async def hypnohub(self, ctx, *tags):
        """Randomly searches Hypnohub for posts."""
        async with ctx.typing():
            await self.booru(ctx, HypnohubBooru, tags)

    @commands.command()
    @commands.is_nsfw()
    async def gelbooru(self, ctx, *tags):
        """Randomly searches Gelbooru for posts."""
        async with ctx.typing():
            await self.booru(ctx, GelBooru, tags)

    @commands.command()
    @commands.is_nsfw()
    async def penis(self, ctx):
        """get penis from e621 bb"""
        await ctx.invoke(self.bot.get_command("e621"), "penis")


async def setup(bot):
    await bot.add_jose_cog(NSFW)
