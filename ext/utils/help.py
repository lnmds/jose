from discord.ext.commands import DefaultHelpCommand
from discord import Forbidden


class PMHelpCommand(DefaultHelpCommand):
    """Reimplementing pm_help=None in a class."""

    async def send_pages(self):
        user = self.context.author
        pages = list(self.paginator.pages)

        # for a single page, just return it
        if len(pages) == 1:
            await self.context.send(pages[0])
            return

        # for multiple pages, always dm
        for index, page in enumerate(pages):
            try:
                await user.send(page)
            except Forbidden:
                await self.context.send(
                    f"failed to DM help page ({index}/{len(pages)})."
                )
