FROM python:3.13

RUN pip install pipenv

ADD ./Pipfile /jose/Pipfile
ADD ./Pipfile.lock /jose/Pipfile.lock

WORKDIR /jose
RUN pipenv install

ADD ./jcoin/Pipfile /jose/jcoin/Pipfile
ADD ./jcoin/Pipfile.lock /jose/jcoin/Pipfile.lock

WORKDIR /jose/jcoin
RUN pipenv install

WORKDIR /jose
